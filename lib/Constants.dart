import 'package:flutter/material.dart';

class Constants {
  static const TextStyle heading1 = TextStyle(
    fontSize: 132,
    color: Colors.grey,
  );

  static const TextStyle richText = TextStyle(
    fontSize: 42,
    fontWeight: FontWeight.w500,
    letterSpacing: 1.26,
    color: Color(0xFF2D3748),
    fontFamily: 'Lato',
  );
}
