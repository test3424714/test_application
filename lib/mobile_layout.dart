import 'package:flutter/material.dart';
import 'package:test_application/Constants.dart';
import 'package:test_application/widgets/tabs.dart';

Widget buildMobileLayout(BuildContext context) {
  return SingleChildScrollView(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(height: MediaQuery.of(context).size.height * 0.05),
        Center(
          child: RichText(
            textAlign: TextAlign.center,
            text: const TextSpan(
              children: [
                TextSpan(
                  text: 'Deine Job\n',
                  style: Constants.richText,
                ),
                TextSpan(
                  text: 'website',
                  style: Constants.richText,
                ),
              ],
            ),
          ),
        ),
        Image.asset(
          'undraw_agreement_aajr.png',
        ),
        Container(
          height: MediaQuery.of(context).size.height * 0.14,
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
            boxShadow: [
              BoxShadow(
                color: Color(0x29000000),
                offset: Offset(0, 3),
                blurRadius: 6,
                spreadRadius: 0,
              ),
            ],
          ),
          child: Center(
            child: Container(
              width: MediaQuery.of(context).size.width * 0.50,
              height: MediaQuery.of(context).size.height * 0.05,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                gradient: const LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color(0xFF319795),
                    Color(0xFF3182CE),
                  ],
                ),
              ),
              child: const Center(
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    'Kostenlos Registrieren',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ),
          ),
        ),
        SizedBox(height: MediaQuery.of(context).size.height * 0.03),
        Tabs(),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: RichText(
            textAlign: TextAlign.center,
            text: const TextSpan(
              children: [
                TextSpan(
                  text: 'Drei einfache Schritte zur \n',
                  style: TextStyle(
                    fontSize: 26,
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.26,
                    color: Color(0xFF2D3748),
                    fontFamily: 'Lato',
                  ),
                ),
                TextSpan(
                  text: 'Vermittlung neuer Mitarbeiter',
                  style: TextStyle(
                    fontSize: 26,
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.26,
                    color: Color(0xFF2D3748),
                    fontFamily: 'Lato',
                  ),
                ),
              ],
            ),
          ),
        ),
        Row(
          children: [
            SizedBox(width: MediaQuery.of(context).size.width * 0.15),
            const Padding(
              padding: EdgeInsets.only(top: 32.0),
              child: Text(
                '1.',
                style: Constants.heading1,
              ),
            ),
            Column(
              children: [
                Image.asset(
                  'undraw_Profile_data_re_v81r.png',
                ),
                SizedBox(height: MediaQuery.of(context).size.height * 0.01),
                Text(
                  'Erstellen dein \n Unternehmensprofil',
                  style: TextStyle(color: Colors.grey),
                )
              ],
            ),
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  '2.',
                  style: Constants.heading1,
                ),
                Text(
                  'Erhalte Vermittlungs- \nangebot von Arbeitgeber',
                  style: TextStyle(color: Colors.grey),
                )
              ],
            ),
            Image.asset(
              'undraw_job_offers_kw5d.png',
            ),
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  '3.',
                  style: Constants.heading1,
                ),
                Text(
                  'Vermittlung nach\nProvision oder \nStundenlohn',
                  style: TextStyle(color: Colors.grey),
                ),
              ],
            ),
            Image.asset(
              'undraw_business_deal_cpi9.png',
            ),
          ],
        )
      ],
    ),
  );
}

class Section extends StatelessWidget {
  const Section({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(width: MediaQuery.of(context).size.width * 0.15),
        const Padding(
          padding: EdgeInsets.only(top: 32.0),
          child: Text(
            '1.',
            style: Constants.heading1,
          ),
        ),
        Column(
          children: [
            Image.asset(
              'undraw_Profile_data_re_v81r.png',
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.01),
            Text(
              'Erstellen dein \n Unternehmensprofil',
              style: TextStyle(color: Colors.grey),
            )
          ],
        ),
      ],
    );
  }
}
