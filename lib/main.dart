import 'dart:js';

import 'package:flutter/material.dart';
import 'package:test_application/mobile_layout.dart';
import 'package:test_application/web_layout.dart';
import 'package:test_application/widgets/appbar.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: buildAppBar(context),
          body: LayoutBuilder(
            builder: (context, constraints) {
              if (constraints.maxWidth < 800) {
                return buildMobileLayout(context);
              } else {
                return buildWebLayout(context);
              }
            },
          ),
        ),
      );
}
