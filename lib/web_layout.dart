import 'package:flutter/material.dart';
import 'package:test_application/Constants.dart';
import 'package:test_application/widgets/tabs.dart';

Widget buildWebLayout(BuildContext context) {
  return SingleChildScrollView(
    child: Column(
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: MediaQuery.of(context).size.height * 0.10),
            Row(
              children: [
                SizedBox(width: MediaQuery.of(context).size.width * 0.15),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    RichText(
                      textAlign: TextAlign.center,
                      text: const TextSpan(
                        children: [
                          TextSpan(
                            text: 'Deine Job\n',
                            style: Constants.richText,
                          ),
                          TextSpan(
                            text: 'website',
                            style: Constants.richText,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height * 0.10),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.20,
                      height: MediaQuery.of(context).size.height * 0.05,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        gradient: const LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                            Color(0xFF319795),
                            Color(0xFF3182CE),
                          ],
                        ),
                      ),
                      child: const Center(
                        child: Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Text(
                            'Kostenlos Registrieren',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(width: MediaQuery.of(context).size.width * 0.10),
                ClipOval(
                  child: Image.asset(
                    'undraw_agreement_aajr.png',
                    height: 400,
                    width: 400,
                    fit: BoxFit
                        .cover, // Ensure the image covers the circular bounds
                  ),
                ),
              ],
            ),
          ],
        ),
        SizedBox(height: MediaQuery.of(context).size.height * 0.10),
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: MediaQuery.of(context).size.height * 0.03),
            Tabs(),
            SizedBox(height: MediaQuery.of(context).size.height * 0.05),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: RichText(
                textAlign: TextAlign.center,
                text: const TextSpan(
                  children: [
                    TextSpan(
                      text: 'Drei einfache Schritte zur\n',
                      style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.w500,
                        letterSpacing: 1.26,
                        color: Color(0xFF2D3748),
                        fontFamily: 'Lato',
                      ),
                    ),
                    TextSpan(
                      text: 'Vermittlung neuer Mitarbeiter',
                      style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.w500,
                        letterSpacing: 1.26,
                        color: Color(0xFF2D3748),
                        fontFamily: 'Lato',
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                SizedBox(width: MediaQuery.of(context).size.width * 0.25),
                const Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text('1.', style: Constants.heading1),
                  ],
                ),
                const Text(
                  'Erstellen dein Lebenslauf.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                ),
                SizedBox(width: MediaQuery.of(context).size.width * 0.10),
                Image.asset(
                  'undraw_Profile_data_re_v81r.png',
                ),
              ],
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.10),
            Row(
              children: [
                SizedBox(width: MediaQuery.of(context).size.width * 0.25),
                const Text(
                  '2.',
                  style: Constants.heading1,
                ),
                const Text(
                  'Erhalte Vermittlungs- \nangebot von Arbeitgeber',
                  style: TextStyle(color: Colors.grey),
                ),
                SizedBox(width: MediaQuery.of(context).size.width * 0.10),
                Image.asset(
                  'undraw_job_offers_kw5d.png',
                ),
              ],
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.10),
            Row(
              children: [
                SizedBox(width: MediaQuery.of(context).size.width * 0.25),
                const Text(
                  '3.',
                  style: Constants.heading1,
                ),
                const Text(
                  'Vermittlung nach\nProvision oder \nStundenlohn',
                  style: TextStyle(color: Colors.grey),
                ),
                SizedBox(width: MediaQuery.of(context).size.width * 0.10),
                Image.asset(
                  'undraw_business_deal_cpi9.png',
                )
              ],
            ),
          ],
        ),
      ],
    ),
  );
}
